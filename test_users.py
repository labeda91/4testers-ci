from users import User


def test_user_initial_state():
    test_user = User("test@example.com", "Password")
    assert test_user.email == "test@example.com"
    assert test_user.password == "Password"
    assert not test_user.logged_in


def test_successful_login():
    test_user = User("test@example.com", "Password")
    test_user.login("Password")
    assert test_user.logged_in