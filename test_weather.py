import os
import requests


def test_check_weather_in_a_city():
    latitude = os.getenv("CITY_LATITUDE", 52.53)
    longitude = os.getenv("CITY_LONGITUDE", 20.73)
    weather_url = "https://api.open-meteo.com/v1/forecast"
    params = {"latitude": latitude, "longitude": longitude}
    get_weather_for_city_response = requests.get(weather_url, params=params)
    weather_for_city = get_weather_for_city_response.json()
    print(weather_for_city)
    assert get_weather_for_city_response.status_code == 200